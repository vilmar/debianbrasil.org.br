---
title: "Legendas para vídeos"
kind: page
created_at: 2020-09-13 20:09
author: Paulo Henrique de Lima Santana
---

# Legendas para vídeos

Projeto para criação de legendas em português de vídeos relativos ao
[Debian](https://www.debian.org).
A princípio, vídeos da [DebConf](https://www.debconf.org/), mas não restrito a
estes. Vale salientar que vídeos em português devem ser legendados também, para que a comunidade surdo-muda possa se beneficiar.

## Informações do time de vídeo do Debian

<https://wiki.debian.org/Teams/DebConf/Video/Subtitles>

Para trabalhar com legendas online, você pode participar do time de vídeo no
Amara:

<https://amara.org/en/teams/debconf/>

## DebConf5

- What is free software - Gunnar Wolf
- Evaluating Open Source Software for Enterprise Use - Martin Langhoff
