---
title: "Usando o BTS para checar traduções em pacotes"
kind: page
created_at: 2020-09-13 20:09
author: Paulo Henrique de Lima Santana
---

## Usando o BTS para checar traduções em pacotes

Nesta pequena seção apresentaremos um simples tutorial de como proceder para
verificar se alguém já efetuou alguma tradução para o mantenedor de um pacote
no qual voluntários gostariam de trabalhar.

O Debian possui um Sistema de Gerenciamento de Bugs (BTS) através do qual
enviamos detalhes de bugs reportados por usuários e desenvolvedores. Cada bug é
associado a um número e é mantido no arquivo até que seja marcado como tendo
sido trabalhado.

Assim sendo, utilizaremos a página: <http://www.debian.org/Bugs> para checar se
o mantenedor já recebeu alguma tradução através de Bugs abertos para o mesmo.

Vá para *Visualizando relatórios de bugs na WWW*

Encontre um bug pelo seu número:

Selecione a opção mantenedor email

Preencha o campo Pelo que procurar com o email do mantenedor do pacote e clique
em *find*.

Aguarde alguns segundos e pronto. Se existir algum **Bug aberto** para pacotes
mantidos por este mantenedor, os mesmos serão apresentados. Confirme o que
deseja e acesse o link dele.

Verifique os Bug´s com **severidade** *wishlist* e encontre as informações necessárias.

## Atenção

Esta etapa é muito importante para que você não faça nenhum trabalho duplicado,
desperdiçando seus esforços com algo que já foi ou está sendo traduzido!