include Nanoc::Helpers::Blogging
include Nanoc::Helpers::LinkTo
include Nanoc::Helpers::Text

def post_date(item, format="%d/%m/%Y")
  Time.parse(item[:created_at]).strftime(format)
end
